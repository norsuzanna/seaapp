angular.module('App', ['ionic', 'ngCordova'])

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('tab', {
            url: '/tab',
            abstract: true,
            templateUrl: 'components/tab/tab.html'
        })

    .state('tab.home', {
        url: '/home',
        views: {
            'tab-home': {
                templateUrl: 'components/home/home.html',
                controller: 'HomeController',
                controllerAs: 'homeCtrl'
            }
        }
    })

    .state('tab.keyword', {
        url: '/keyword',
        views: {
            'tab-keyword': {
                templateUrl: 'components/news/keyword/keyword.html',
                controller: 'ChartController',
                controllerAs: 'chartCtrl'
            }
        }
    })

    .state('tab.chart-detail', {
        url: '/keyword/:listId',
        views: {
            'tab-keyword': {
                templateUrl: 'components/news/comparison/comparison.html',
                controller: 'ChartDetailController',
                controllerAs: 'chartDetailCtrl'
            }
        }
    })
    $urlRouterProvider.otherwise('/tab/home');

});
