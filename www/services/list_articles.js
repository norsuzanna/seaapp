(function() {
    'use strict';

    angular
        .module('App')
        .factory('ListArticles', ListArticles);

    ListArticles.$inject = [];

    function ListArticles() {
        var list = [{
            id: 0,
            initial: 'A',
            keyword: 'Adam Rosly',
            date: '22th April 2017',
            time: '11.13pm',
            news: [{
                name: 'Berita Harian',
                title: 'Lorem ipsum',
                value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut tellus in sapien blandit pellentesque. Phasellus quis urna nec lacus aliquam elementum. Curabitur at dolor justo. Suspendisse sodales purus tortor, id eleifend tortor venenatis et. Duis interdum nisl sit amet dapibus pellentesque. Pellentesque pharetra lorem ut viverra ullamcorper. Nam id enim et mi rutrum scelerisque vitae eget dui. Sed turpis urna, euismod mattis fermentum nec, blandit vel ante. Nam ullamcorper elit et nisl ornare vulputate. Aenean arcu lacus, auctor malesuada neque hendrerit, blandit porttitor felis. Aliquam convallis sagittis est ut ultrices. Nulla pulvinar condimentum sodales. Nulla fermentum magna eget quam hendrerit, vel tempor augue vulputate. Pellentesque in erat eleifend, interdum odio non, aliquet tellus. Maecenas a erat vitae diam euismod ullamcorper. Quisque diam libero, facilisis at maximus ac, viverra eget neque.'
            }, {
                name: 'Sinar',
                title: 'Quisque iaculis',
                value: 'Quisque iaculis tincidunt enim, vel tempor sapien venenatis sed. Donec porta rhoncus ipsum, ut accumsan nunc volutpat ac. Proin dapibus felis est, id iaculis ipsum cursus nec. Proin at odio laoreet, feugiat massa vitae, fermentum purus. Praesent nibh elit, interdum in volutpat id, consectetur sed lectus. In eleifend tristique metus, in vestibulum mi pulvinar non. Maecenas pharetra urna odio, ut accumsan ante posuere vitae. Fusce et eros et quam hendrerit volutpat. Morbi varius nec erat ut iaculis. Proin ac odio et risus lobortis tincidunt. Praesent enim neque, pharetra a urna at, rutrum venenatis felis. Quisque nunc tortor, feugiat at ligula eget, tempus volutpat ex. Donec consequat turpis nibh, ultrices venenatis sem efficitur feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In id dignissim nisl.'
            }, {
                name: 'Utusan Malaysia',
                title: 'Donec at',
                value: 'Donec at quam nec eros auctor eleifend. Quisque tincidunt ut magna cursus accumsan. Quisque tincidunt quis justo vitae pretium. Sed vehicula nec purus porta pellentesque. Sed non molestie elit. Etiam auctor laoreet nisl quis scelerisque. Proin dui ligula, lacinia quis dolor id, vulputate ultricies purus. Nullam eleifend ante non dolor interdum tempor.'
            }]
        }];

        return {
            all: function() {
                return list;
            },
            remove: function(list) {
                list.splice(chats.indexOf(list), 1);
            },
            get: function(listId) {
                for (var i = 0; i < list.length; i++) {
                    if (list[i].id === parseInt(listId)) {
                        return list[i];
                    }
                }
                return null;
            }
        };
    }
})();
