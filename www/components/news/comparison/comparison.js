(function() {
    'use strict';

    angular
        .module('App')
        .controller('ChartDetailController', ChartDetailController);

    ChartDetailController.$inject = ['$scope', '$stateParams', 'ListArticles'];

    function ChartDetailController($scope, $stateParams, ListArticles) {
        $scope.list = ListArticles.get($stateParams.listId);
        $scope.news = [];
        $scope.activeSlide = 0;

        angular.forEach($scope.list.news, function(value, key) {
            $scope.news.push(value.name);
            $scope.news.push(value.title);
            $scope.news.push(value.value);
        });

        console.log($scope.news);
    }

})();
