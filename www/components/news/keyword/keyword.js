(function() {
    'use strict';

    angular
        .module('App')
        .controller('ChartController', ChartController);

    ChartController.$inject = ['$scope', 'ListArticles'];

    function ChartController($scope, ListArticles) {
    	$scope.lists = ListArticles.all();
        $scope.remove = function(list) {
            ListArticles.remove(list);
        };
    }
})();